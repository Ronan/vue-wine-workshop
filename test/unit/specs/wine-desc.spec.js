/*
import Vue from 'vue'
import { expect } from 'chai'

import WineDesc from 'src/components/wine-desc'

function promise (data) {
  return {
    then (func) {
      return promise(func(data))
    },
    catch () {}
  }
}

function fetchWine (wine) {
  return (url) => {
    if (url.search('/api') === 0) {
      if (url.search('/api/wines/cheval-noir') === 0) {
        return promise({ text: () => wine })
      }
    }
    throw new Error('Unknown URL : ' + url)
  }
}

describe('wine-desc.vue', () => {
  const empty = 'No wine found'
  const wine = '{\n' +
               '  "id": "cheval-noir",\n' +
               '  "name": "Cheval Noir",\n' +
               '  "type": "Rouge",\n' +
               '  "appellation": {"name": "Saint-Emilion", "region": "Bordeaux"},\n' +
               '  "grapes": ["Cabernet Sauvignon", "Merlot", "Cabernet Franc"]\n' +
               '}'

  it('should not display a wine image when empty', () => {
    const vm = new Vue({
      template: '<div><wine-desc></wine-desc></div>',
      components: { WineDesc }
    }).$mount()
    expect(vm.$el.querySelectorAll('img').length).to.equal(0)
  })

  it('should display a message when empty', () => {
    const vm = new Vue({
      template: '<div><wine-desc></wine-desc></div>',
      components: { WineDesc }
    }).$mount()
    expect(vm.$el.querySelectorAll('span').length).to.equal(1)
    expect(vm.$el.querySelector('span').innerText).to.equal(empty)
  })

  it('should display wine info', (done) => {
    window.fetch = fetchWine(wine)
    const vm = new Vue({
      template: '<div><wine-desc></wine-desc></div>',
      components: { WineDesc }
    }).$mount()

    vm.$broadcast('loadwine', 'cheval-noir')

    vm.$nextTick(function () {
      expect(vm.$el.querySelectorAll('img').length).to.equal(1)
      expect(vm.$el.querySelectorAll('div.title').length).to.equal(1)
      expect(vm.$el.querySelector('div.title').innerText).to.equal('Cheval Noir')
      expect(vm.$el.querySelectorAll('div.info').length).to.equal(4)
      expect(vm.$el.querySelectorAll('div.info')[0].innerText).to.contain('Rouge')
      expect(vm.$el.querySelectorAll('div.info')[1].innerText).to.contain('Bordeaux')
      expect(vm.$el.querySelectorAll('div.info')[2].innerText).to.contain('Saint-Emilion')
      expect(vm.$el.querySelectorAll('div.info')[3].innerText).to.contain('Cabernet Sauvignon')
      expect(vm.$el.querySelectorAll('div.info')[3].innerText).to.contain('Merlot')
      expect(vm.$el.querySelectorAll('div.info')[3].innerText).to.contain('Cabernet Franc')
      done()
    })
  })
})
*/
