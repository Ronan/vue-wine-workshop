import { expect } from 'chai'

import Vue from 'vue'
import store from 'src/vuex/store'
import { log } from 'src/vuex/actions'

import Console from 'src/components/console'

describe('console', () => {
  it('should not display any message when empty', () => {
    const vm = new Vue({
      template: '<div><console></console></div>',
      components: { Console },
      store: store
    }).$mount()
    // console.log(vm.$el.innerHTML)
    expect(vm.$el.querySelectorAll('p').length).to.equal(0)
  })

  it('should not display a error message', () => {
    const vm = new Vue({
      template: '<div><console></console></div>',
      components: { Console },
      store: store,
      vuex: {
        actions: {
          log: log
        }
      },
      data: function () {
        this.log().info({source: 'console.spec', content: 'test error message'})
      }
    }).$mount()

    // console.log(vm.$el.innerHTML)
    expect(vm.$el.querySelectorAll('p').length).to.equal(1)
  })
})
