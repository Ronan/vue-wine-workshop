import { expect } from 'chai'

import { setTitle, loadRegions } from 'src/vuex/actions'

// helper for testing action with expected mutations
const testAction = (action, args, state, expectedMutations, done) => {
  let count = 0
  // mock dispatch
  const dispatch = (name, ...payload) => {
    const mutation = expectedMutations[count]
    expect(mutation.name).to.equal(name)
    if (payload) {
      expect(mutation.payload).to.deep.equal(payload)
    }
    count++
    if (count >= expectedMutations.length) {
      done()
    }
  }
  // call the action with mocked store and arguments
  action({dispatch, state}, ...args)

  // check if no mutations should have been dispatched
  if (expectedMutations.length === 0) {
    expect(count).to.equal(0)
    done()
  }
}

function promise (data) {
  return {
    then (func) {
      return promise(func(data))
    },
    catch () {}
  }
}

function fakedFetch (text) {
  return (url) => {
    return promise({ text: () => text, json: () => JSON.parse(text) })
  }
}

describe('setTitle', () => {
  it('setTitle should dispatch a SET_TITLE mutation', (done) => {
    testAction(setTitle, ['New Title'], {}, [
      { name: 'SET_TITLE', payload: ['New Title'] }
    ], done)
  })
})

describe('loadRegions', () => {
  window.fetch = fakedFetch('["Bordeaux","Bourgogne","Champagne"]')

  it('loadRegions should change title, regions and links', (done) => {
    testAction(loadRegions, [], {}, [
      { name: 'SET_TITLE', payload: ['Liste des régions'] },
      { name: 'SET_REGIONS', payload: [ ['Bordeaux', 'Bourgogne', 'Champagne'] ] },
      { name: 'SET_LINKS', payload: [ [{ label: 'Home' }] ] }
    ], done)
  })
})
