import { expect } from 'chai'
import { mutations } from 'src/vuex/store'

const { SET_TITLE, ADD_LOG } = mutations

describe('store', () => {
  it('SET_TITLE should change title', () => {
    const state = { title: 'old title' }
    SET_TITLE(state, 'new title')
    expect(state.title).to.equal('new title')
  })

  it('ADD_LOG add a log', () => {
    const state = { logs: [] }

    ADD_LOG(state, {content: 'new log'})
    ADD_LOG(state, {content: 'another log'})

    expect(state.logs.length).to.equal(2)
    expect(state.logs[0]).to.have.property('date')
    expect(state.logs[0].content).to.equal('new log')
    expect(state.logs[1]).to.have.property('date')
    expect(state.logs[1].content).to.equal('another log')
  })
})
