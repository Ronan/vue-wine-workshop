/*
import Vue from 'vue'
import { expect } from 'chai'

import WineList from 'src/components/wine-list'

function promise (data) {
  return {
    then (func) {
      return promise(func(data))
    },
    catch () {}
  }
}

function fetchWines (wines) {
  return (url) => {
    if (url.search('/api') === 0) {
      if (url.search('/api/wines\\?region=Bordeaux') === 0) {
        return promise({ text: () => wines })
      }
    }
    throw new Error('Unknown URL : ' + url)
  }
}

describe('wine-list.vue', () => {
  const empty = 'List is empty'
  const noWines = '[]'
  const someWines =
      '[' +
      '  {' +
      '    "id": "cheval-noir",' +
      '    "name": "Cheval Noir",' +
      '    "type": "Rouge",' +
      '    "appellation": {"name": "Saint-Emilion", "region": "Bordeaux"},' +
      '    "grapes": ["Cabernet Sauvignon", "Merlot", "Cabernet Franc"]' +
      '  },' +
      '  {' +
      '    "id": "les-hauts-de-tour-prignac",' +
      '    "name": "Les Hauts de Tour Prignac",' +
      '    "type": "Rouge",' +
      '    "appellation": {"name": "Médoc", "region": "Bordeaux"},' +
      '    "grapes": ["Cabernet Sauvignon", "Merlot"]' +
      '  }' +
      ']'

  it('should not display a list when empty', () => {
    window.fetch = fetchWines(noWines)
    const vm = new Vue({
      template: '<div><wine-list></wine-list></div>',
      components: { WineList }
    }).$mount()
//    console.log(vm.$el.innerHTML)
    expect(vm.$el.querySelectorAll('ul').length).to.equal(0)
  })

  it('should display a message when empty', () => {
    window.fetch = fetchWines(noWines)
    const vm = new Vue({
      template: '<div><wine-list empty="' + empty + '" ></wine-list></div>',
      components: { WineList }
    }).$mount()

    expect(vm.$el.querySelectorAll('span').length).to.equal(1)
    expect(vm.$el.querySelector('span').innerText).to.equal(empty)
  })

  it('should display wines', (done) => {
    window.fetch = fetchWines(someWines)
    const vm = new Vue({
      template: '<div><wine-list empty="' + empty + '" ></wine-list></div>',
      components: { WineList }
    }).$mount()

    vm.$broadcast('loadwines', 'Bordeaux')

    vm.$nextTick(function () {
      expect(vm.$el.querySelectorAll('ul').length).to.equal(1)
      expect(vm.$el.querySelectorAll('li').length).to.equal(2)
      expect(vm.$el.querySelectorAll('li')[0].innerText).to.equal('Cheval Noir')
      expect(vm.$el.querySelectorAll('li')[1].innerText).to.equal('Les Hauts de Tour Prignac')
      done()
    })
  })

  it('Should allow selection of 1 item', (done) => {
    window.fetch = fetchWines(someWines)
    const vm = new Vue({
      template: '<div><wine-list empty="' + empty + '" ></wine-list></div>',
      components: { WineList }
    }).$mount()

    vm.$broadcast('loadwines', 'Bordeaux')

    vm.$nextTick(function () {
      vm.$el.querySelectorAll('li')[1].click()
      vm.$nextTick(function () {
        expect(vm.$el.querySelectorAll('li').length).to.equal(2)
        expect(vm.$el.querySelectorAll('li.selected').length).to.equal(1)
        expect(vm.$el.querySelectorAll('li')[1].className).to.equal('selected')
        done()
      })
    })
  })

  it('Should dispatch a change event when an item is clicked', (done) => {
    window.fetch = fetchWines(someWines)
    const changed = sinon.spy()
    const vm = new Vue({
      template: '<div><wine-list empty="' + empty + '" v-on:change="onchange" ></wine-list></div>',
      components: { WineList },
      methods: { onchange: (item) => changed(item) }
    }).$mount()

    vm.$broadcast('loadwines', 'Bordeaux')

    expect(changed.calledOnce).to.equal(false)
    vm.$nextTick(function () {
      expect(changed.calledOnce).to.equal(false)
      vm.$el.querySelectorAll('li')[1].click()
      vm.$nextTick(function () {
        expect(changed.withArgs('les-hauts-de-tour-prignac').calledOnce).to.equal(true)
        done()
      })
    })
  })

  it('Should reset selection when loading', (done) => {
    window.fetch = fetchWines(someWines)
    const vm = new Vue({
      template: '<div><wine-list empty="' + empty + '" ></wine-list></div>',
      components: { WineList }
    }).$mount()

    vm.$broadcast('loadwines', 'Bordeaux')

    vm.$nextTick(function () {
      vm.$el.querySelectorAll('li')[1].click()
      vm.$nextTick(function () {
        expect(vm.$el.querySelectorAll('li.selected').length).to.equal(1)
        vm.$broadcast('loadwines', 'Bordeaux')
        vm.$nextTick(function () {
          expect(vm.$el.querySelectorAll('li.selected').length).to.equal(0)
          done()
        })
      })
    })
  })
})
*/
