/*
import Vue from 'vue'
import { expect } from 'chai'
import RegionList from 'src/components/region-list'

function promise (data) {
  return {
    then (func) {
      return promise(func(data))
    },
    catch () {}
  }
}

function fetchRegions (regions) {
  return (url) => {
    if (url.search('/api') === 0) {
      if (url.search('/api/regions') === 0) {
        return promise({ text: () => regions })
      }
    }
    throw new Error('Unknown URL : ' + url)
  }
}

describe('region-list.vue', () => {
  const empty = 'No region found'
  const noRegions = '[]'
  const someRegions = '["Bordeaux","Bourgogne","Champagne"]'

  it('should not display a list when empty', () => {
    window.fetch = fetchRegions(noRegions)
    const vm = new Vue({
      template: '<div><region-list></region-list></div>',
      components: { RegionList }
    }).$mount()
    // console.log(vm.$el.innerHTML)
    expect(vm.$el.querySelectorAll('ul').length).to.equal(0)
  })

  it('should display a message when empty', () => {
    window.fetch = fetchRegions(noRegions)
    const vm = new Vue({
      template: '<div><region-list></region-list></div>',
      components: { RegionList }
    }).$mount()
    // console.log(vm.$el.innerHTML)
    expect(vm.$el.querySelectorAll('span').length).to.equal(1)
    expect(vm.$el.querySelector('span').innerText).to.equal(empty)
  })

  it('should display regions', () => {
    window.fetch = fetchRegions(someRegions)
    const vm = new Vue({
      template: '<div><region-list></region-list></div>',
      components: { RegionList }
    }).$mount()
    // console.log(vm.$el.innerHTML)
    expect(vm.$el.querySelectorAll('ul').length).to.equal(1)
    expect(vm.$el.querySelectorAll('li').length).to.equal(3)
    expect(vm.$el.querySelectorAll('li')[0].innerText).to.contain('Bordeaux')
    expect(vm.$el.querySelectorAll('li')[1].innerText).to.contain('Bourgogne')
    expect(vm.$el.querySelectorAll('li')[2].innerText).to.contain('Champagne')
  })
})
*/
