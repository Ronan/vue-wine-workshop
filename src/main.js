import Vue from 'vue'
import VueRouter from 'vue-router'
import RegionList from './components/region-list'
import WineList from './components/wine-list'
import WineDesc from './components/wine-desc'

Vue.use(VueRouter)
var router = new VueRouter()
router.map({
  '/': {
    name: 'regions',
    component: RegionList
  },
  '/regions/:regionId': {
    name: 'wines',
    component: WineList
  },
  '/regions/:regionId/wines/:wineId': {
    name: 'description',
    component: WineDesc
  }
})

import App from './App'
router.start(App, 'body')
