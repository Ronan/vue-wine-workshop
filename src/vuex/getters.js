export function getTitle (state) {
  return state.title
}

export function getLinks (state) {
  return state.links
}

export function getRegions (state) {
  return state.regions
}

export function getWines (state) {
  return state.wines
}

export function getWine (state) {
  return state.wine
}

export function isLiked (state) {
  return state.liked
}

export function getComments (state) {
  return state.comments
}

export function getLogs (state) {
  return state.logs
}
