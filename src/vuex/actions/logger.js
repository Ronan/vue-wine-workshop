export const logger = ({dispatch}) => {
  return {
    error: (msg) => logError({dispatch}, msg),
    info: (msg) => logInfo({dispatch}, msg),
    warn: (msg) => logWarn({dispatch}, msg),
    trace: (msg) => logTrace({dispatch}, msg)
  }
}

const logError = ({dispatch}, msg) => {
  logMsg({dispatch}, 'ERROR', msg)
}

const logWarn = ({dispatch}, msg) => {
  logMsg({dispatch}, 'WARN', msg)
}

const logInfo = ({dispatch}, msg) => {
  logMsg({dispatch}, 'INFO', msg)
}

const logTrace = ({dispatch}, msg) => {
  logMsg({dispatch}, 'TRACE', msg)
}

const logMsg = ({dispatch}, level, msg) => {
  dispatch('ADD_LOG', {...msg, type: level})
}
