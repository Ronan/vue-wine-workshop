export const post = (url, content, success, error) => {
  window.fetch(
    url,
    {
      method: 'post',
      headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
      body: JSON.stringify(content)
    })
    .then(text => text.json())
    .then(json => success(json))
    .catch(ex => error(ex))
}

export const fetch = (url, success, error) => {
  window.fetch(url)
    .then(text => text.json())
    .then(json => success(json))
    .catch(ex => error(ex))
}
