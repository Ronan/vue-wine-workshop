import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
  title: 'Wine App',
  links: [{ label: 'Home' }],
  regions: null,
  wines: null,
  wine: null,
  liked: false,
  comments: [],
  lastLog: null,
  logs: []
}

export const mutations = {
  SET_TITLE (state, title) {
    state.title = title
  },
  SET_LINKS (state, links) {
    state.links = links
  },
  SET_REGIONS (state, regions) {
    state.regions = regions
  },
  SET_WINES (state, wines) {
    state.wines = wines
  },
  SET_WINE (state, wine) {
    state.wine = wine
  },
  SET_LIKED (state, liked) {
    state.liked = liked
  },
  SET_COMMENTS (state, comments) {
    state.comments = comments
  },
  ADD_LOG (state, log) {
    state.logs.push({...log, date: new Date()})
  }
}

export default new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  state,
  mutations
})
