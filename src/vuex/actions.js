import { logger } from './actions/logger'
import { post, fetch } from './actions/apicall'

export const log = logger

export const setTitle = ({dispatch}, title) => {
  dispatch('SET_TITLE', title)
}

export const loadRegions = ({dispatch}) => {
  fetch('/api/regions',
    json => {
      dispatch('SET_TITLE', 'Liste des régions')
      dispatch('SET_REGIONS', json)
      dispatch('SET_LINKS', [{ label: 'Home' }])
    },
    error => log({dispatch}).error({source: 'loadRegions', content: 'Cannot load regions : ' + error})
  )
}

export const loadWines = ({dispatch}, regionId) => {
  fetch('/api/wines?region=' + regionId,
    json => {
      dispatch('SET_TITLE', 'Liste des vins de ' + regionId)
      dispatch('SET_WINES', json)
      dispatch(
        'SET_LINKS',
        [
          { label: 'Home', path: '/' },
          { label: regionId }
        ]
      )
    },
    error => log({dispatch}).error({source: 'loadWines', content: 'Cannot load wines : ' + error})
  )
}

export const loadWine = ({dispatch}, wineId) => {
  loadWineInfos({dispatch}, wineId)
  loadWineLike({dispatch}, wineId)
  loadWineComments({dispatch}, wineId)
}

const loadWineInfos = ({dispatch}, wineId) => {
  fetch('/api/wines/' + wineId,
    json => {
      dispatch('SET_TITLE', 'Description du ' + json.name)
      dispatch('SET_WINE', json)
      dispatch(
        'SET_LINKS',
        [
          { label: 'Home', path: '/' },
          { label: json.appellation.region, path: '/regions/' + json.appellation.region },
          { label: json.name }
        ]
      )
    },
    error => log({dispatch}).error({source: 'loadWineInfos', content: 'Cannot load wine : ' + error})
  )
}

const loadWineLike = ({dispatch}, wineId) => {
  fetch('/api/wines/' + wineId + '/like',
    json => dispatch('SET_LIKED', json.like),
    error => log({dispatch}).error({source: 'loadWineLike', content: 'Cannot load like : ' + error})
  )
}

const loadWineComments = ({dispatch}, wineId) => {
  fetch('/api/wines/' + wineId + '/comments',
    json => dispatch('SET_COMMENTS', json),
    error => log({dispatch}).error({source: 'loadWineComments', content: 'Cannot load comments : ' + error})
  )
}

export const changeLiked = ({dispatch}, wineId, like) => {
  dispatch('SET_LIKED', like)
  post(
    '/api/wines/' + wineId + '/like',
    { like: like },
    () => {},
    error => log({dispatch}).error({source: 'changeLiked', content: 'Cannot change liked : ' + error})
  )
}

export const addComment = ({dispatch}, wineId, title, body) => {
  post(
    '/api/wines/' + wineId + '/comments',
    { title: title, content: body },
    () => loadWineComments({dispatch}, wineId),
    error => log({dispatch}).error({source: 'addComment', content: 'Cannot add comment : ' + error})
  )
}
